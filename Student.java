public class Student
{
	public String name;
	public int lockerNumber;
	public int studentID;
	
	public void sayHi()
	{
		System.out.println("Hello!");
	}
	
	public void openLocker()
	{
		System.out.println(this.name + " opened his locker.");
	}
	
	public void closeLocker()
	{
		System.out.println(this.name + " closed locker " + this.lockerNumber);
	}
}