public class Application
{
	public static void main(String[] args)
	{
		Student one = new Student();
		Student two = new Student();
		
		one.name ="Ahmed";
		//System.out.println(one.name);
		one.lockerNumber=250;
		//System.out.println(one.lockerNumber);
		one.studentID=2134222;
		//System.out.println(one.studentID);
		
		two.name="David";
		//System.out.println(two.name);
		two.lockerNumber=175;
		//System.out.println(two.lockerNumber);
		two.studentID=2134678;
		//System.out.println(two.studentID);
		
		one.sayHi();
		one.openLocker();
		one.closeLocker();
		
		two.sayHi();
		two.openLocker();
		two.closeLocker();
		
		Student[] section3 = new Student[3];
		section3[0] = one;
		section3[1] = two;
		
		System.out.println(section3[0].name + " Step 15 Trying out the print");
		
		section3[2] = new Student();
		section3[2].name = "John";
		section3[2].lockerNumber = 123;
		section3[2].studentID = 2145533;
		
		System.out.println(section3[2].name);
		System.out.println(section3[2].lockerNumber);
		System.out.println(section3[2].studentID);
	}
}
